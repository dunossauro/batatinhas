from unittest import TestCase, mock
from flask import url_for
from app import app
from app.model import Usuario


class FirstTestCase(TestCase):
    def setUp(self):
        self.app = app
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()
        self.app.testing = True

    def test_root_should_return_200(self):
        result = self.client.get(url_for('crud.root'))

        self.assertEqual(200, result.status_code)

    @mock.patch('app.crud.session')
    def test_get_users(self, m_session):
        user = {'nome': 'Eduardo', 'sobrenome': 'Mendes'}
        m_session.query(Usuario).all.return_value = [Usuario(**user)]

        result = self.client.get(url_for('crud.get_users'))

        result_user = result.json[0]
        result_user.pop('id')
        self.assertEqual(user, result_user)

    @mock.patch('app.crud.session')
    def test_post_user(self, m_session):
        from json import dumps
        request_ob = {'nome': 'Eduardo', 'sobrenome': 'Mendes'}
        result = self.client.post(url_for('crud.post_user'),
                                  data=dumps(request_ob))

        self.assertTrue(m_session.add.called)
        self.assertTrue(m_session.commit.called)
