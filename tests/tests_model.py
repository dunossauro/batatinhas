from unittest import TestCase, mock
from app.model import Usuario

class TestModelUsuario(TestCase):
    def test_table_name_should_return_Usuario(self):
        expected = 'Usuario'
        result = Usuario(nome='Eduardo', sobrenome='Mendes')

        self.assertEqual(expected, result.__tablename__)

    def test_table_repr_should_return_correct_user(self):
        expected = 'Usuario(nome="Eduardo", sobrenome="Mendes")'
        result = Usuario(nome='Eduardo', sobrenome='Mendes')

        self.assertEqual(expected, result.__repr__())
