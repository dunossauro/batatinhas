from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///base.db', echo=False)
Base = declarative_base()

session = scoped_session(sessionmaker(bind=engine, autoflush=False))


class Usuario(Base):
    __tablename__ = 'Usuario'
    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String)
    sobrenome = Column(String)

    def __repr__(self):
        return f'Usuario(nome="{self.nome}", sobrenome="{self.sobrenome}")'

    def as_dict(self):
        return {c.name: getattr(self, c.name)
                for c in self.__table__.columns}


Base.metadata.create_all(engine)
