from flask import Blueprint, jsonify, request, render_template, make_response
from wtforms import Form, StringField, SubmitField
from .model import session, Usuario

crud_bp = Blueprint('crud', __name__)


class User(Form):
    nome = StringField('Nome')
    sobrenome = StringField('sobrenome')
    butt = SubmitField('OK!')


@crud_bp.route('/')
def root():
    return render_template('home.html', form=User())


@crud_bp.route('/get-users')
def get_users():
    users = [u.as_dict() for u in session.query(Usuario).all()]

    resp = make_response(jsonify(users))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@crud_bp.route('/post-user', methods=['POST'])
def post_user():
    user = request.get_json(force=True)
    db_user = Usuario(**user)
    session.add(db_user)
    session.commit()
    return 'ok', 201
