function write_table(users_json){
     var table = document.getElementById('Users')
     table.innerHTML = users_json.map(user =>
          `<tr><td>${user.nome}</td>
           <td>${user.sobrenome}</td></tr>`
      ).join('')
  }

function get_users(){
   fetch('http://127.0.0.1:5000/get-users')
    .then(response => response.json())
    .then(myJSON => write_table(myJSON))
}


function post_user() {
  return fetch('http://127.0.0.1:5000/post-user', {
    body: JSON.stringify({nome: document.getElementById("nome").value,
                          sobrenome: document.getElementById("sobrenome").value}),
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'content-type': 'application/json'
    },
    method: 'POST',
    mode: 'no-cors',
    redirect: 'follow',
    referrer: 'no-referrer',
  })
  .then(response => response.json())
}

get_users()
